---
stages:
  - check
  - verify
  - build
  - publish
  - test

include:
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml

check code style:
  stage: check
  image: registry.gitlab.com/naturalis/lib/maven:3-jdk-11
  script:
    - mvn checkstyle:check -Dcheckstyle.config.location=google_checks.xml
  artifacts:
    paths:
      - target/checkstyle-*
  allow_failure: true

lint dockerfile:
  stage: check
  image: hadolint/hadolint:latest-debian
  before_script:
    - mkdir -p reports
  script:
    - hadolint -f gitlab_codeclimate Dockerfile > reports/hadolint-$(md5sum Dockerfile | cut -d" " -f1).json
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    reports:
      codequality:
        - "reports/*"
    paths:
      - "reports/*"
  allow_failure: true

secret_detection:
  extends: .secret-analyzer
  stage: check
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: "false"
    SECRET_DETECTION_REPORT_FILE: "gl-secret-detection-report.json"
  artifacts:
    reports:
      secret_detection: $SECRET_DETECTION_REPORT_FILE
    expire_in: 7 days
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
    - if: $CI_PIPELINE_SOURCE == "web"
  allow_failure: false
  before_script:
    - apk add --no-cache jq
  script:
    - /analyzer run
    - |
      if [ -f "$SECRET_DETECTION_REPORT_FILE" ]; then
        if [ "$(jq ".vulnerabilities | length" $SECRET_DETECTION_REPORT_FILE)" -gt 0 ]; then
          echo "Vulnerabilities detected!"
          echo "Please check the $SECRET_DETECTION_REPORT_FILE security file."
          echo "Or run gitleak locally on your repository."
          exit 1
        fi
      else
        echo "Artifact $SECRET_DETECTION_REPORT_FILE does not exist."
        echo "No evaluation can be performed."
      fi

maven test:
  stage: verify
  image: maven:3-eclipse-temurin-21
  tags:
    - stackio
  script:
    - mvn verify

maven build:
  stage: build
  image: maven:3-eclipse-temurin-21
  tags:
    - stackio
  script:
    - mvn clean install
  artifacts:
    name: "infuser-${CI_COMMIT_REF_NAME}"
    paths:
      - target

docker image:
  stage: publish
  image: docker:latest
  tags:
    - stackio
  before_script:
    - echo "${CI_JOB_TOKEN}" | docker login "${CI_REGISTRY}" -u "${CI_REGISTRY_USER}" --password-stdin
  script:
    - docker build --pull -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}" .
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}"
    # - docker build --pull -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}" -t "${CI_REGISTRY_IMAGE}:latest" .
    # - docker push "${CI_REGISTRY_IMAGE}:-latest"
  services:
    - docker:dind

container_scanning:
  variables:
    SECURE_LOG_LEVEL: 'error'
    CS_IMAGE: "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_NAME}"

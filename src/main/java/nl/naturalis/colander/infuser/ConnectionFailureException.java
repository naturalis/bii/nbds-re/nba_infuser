package nl.naturalis.colander.infuser;

public class ConnectionFailureException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private static final String PREFIX = "Error while connecting to Elasticsearch cluster.";

  public ConnectionFailureException(String message) {
    super(PREFIX + message);
  }

  public ConnectionFailureException(Throwable cause) {
    super(cause);
  }
}

package nl.naturalis.colander.infuser;

import java.io.IOException;
import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

@SuppressWarnings("CheckStyle")
public class ESClientManager implements AutoCloseable {

  private static final Logger logger = LogManager.getLogger(ESClientManager.class);
  private final LoaderConfig config;
  private RestHighLevelClient client;

  private ESClientManager(LoaderConfig config) {
    this.config = config;
  }

  static ESClientManager newInstance(LoaderConfig config) {
    return new ESClientManager(config);
  }

  /**
   * Returns an Elasticsearch {@link RestHighLevelClient} instance.
   *
   * @return RestHighLevelClient
   */
  public synchronized RestHighLevelClient getClient() {

    if (client == null) {
      logger.info("Connecting to Elasticsearch cluster");
      String host = config.getHost();
      int port = config.getPort();
      String scheme = config.getScheme();
      client = new RestHighLevelClient(
          RestClient.builder(
              new HttpHost(host, port, scheme))
      );
      logger.info("Connected");
    }
    ping();
    return client;
  }

  private synchronized void closeClient() {
    if (client != null) {
      logger.info("Disconnecting from Elasticsearch cluster");
      try {
        client.close();
      } catch (IOException e) {
        client = null;
        e.printStackTrace();
      } finally {
        client = null;
      }
    }
  }

  /**
   * Ping the host
   *
   */
  private void ping() {
     try {
      client.ping(RequestOptions.DEFAULT);
      logger.info("Elasticsearch is up and running");
    } catch (IOException e) {
      String msg = "Elasticsearch is offline!";
      throw new ConnectionFailureException(msg);
    }
  }

  @Override
  public void close() {
    closeClient();
  }
}

package nl.naturalis.colander.infuser;

public class InitializationException extends RuntimeException {

  /**
   * InitializationException ... .
   */
  private static final long serialVersionUID = 1L;

  public InitializationException(String message) {
    super(message);
  }

  public InitializationException(Throwable cause) {
    super(cause);
  }

  public InitializationException(String message, Throwable cause) {
    super(message, cause);
  }
}

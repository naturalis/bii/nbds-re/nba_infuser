package nl.naturalis.colander.infuser;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static nl.naturalis.colander.infuser.model.DocumentType.GEOAREA;
import static nl.naturalis.colander.infuser.model.DocumentType.MULTIMEDIAOBJECT;
import static nl.naturalis.colander.infuser.model.DocumentType.SPECIMEN;
import static nl.naturalis.colander.infuser.model.DocumentType.TAXON;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingFormatArgumentException;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import nl.naturalis.colander.infuser.model.Alias;
import nl.naturalis.colander.infuser.model.DocumentType;
import nl.naturalis.colander.infuser.model.Job;
import nl.naturalis.colander.infuser.model.JobSummary;
import nl.naturalis.colander.infuser.model.SourceSystem;
import nl.naturalis.colander.infuser.model.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.ActionRequestValidationException;
import org.elasticsearch.client.RestHighLevelClient;

/**
 * NBA Document Loader.
 *
 * @author Tom Gilissen
 */
public class Loader {

  private static final Logger logger = LogManager.getLogger(Loader.class);
  private static final Job job = Job.getInstance();
  private static ObjectMapper objectMapper;
  private static TextMessenger messenger;
  private static File errorDirectory;

  private final LoaderConfig config;
  private final File jobsDirectory;
  private final File incomingDirectory;
  private final File archiveDirectory;
  private RestHighLevelClient client;
  private int batchSize;
  private final ArrayList<String> batch = new ArrayList<>(batchSize + 11);
  private BulkIndexer indexer;
  private File jobFile;
  private String dataSupplier;
  private final boolean cleanUp;

  /**
   * Loader class.
   *
   * @throws IOException when the LoaderConfig fails
   */
  public Loader() throws IOException {
    objectMapper = new ObjectMapper();
    config = LoaderConfig.getInstance();
    jobsDirectory = config.getJobsDirectory();
    incomingDirectory = config.getIncomingDirectory();
    archiveDirectory = config.getArchiveDirectory();
    batchSize = config.getBatchSize();
    cleanUp = config.cleanUp();
    messenger = new TextMessenger(config);
  }

  /**
   * Main class, start here.
   *
   * @param args optional arguments
   *
   * @throws IOException when writing the log file fails
   */
  public static void main(String[] args) throws IOException {

    int exitStatus = 0;
    String duration = "0 seconds";
    boolean sendTextMessages = false;
    Loader loader;

    try {
      loader = new Loader();
      sendTextMessages = loader.config.sendTextMessages();
      job.setSendTextMessage(sendTextMessages);
      errorDirectory = loader.archiveDirectory;
      if (args != null && args.length > 0) {
        loader.run(args);
      } else {
        loader.run();
      }
    } catch (IOException e) {

      logger.error("FAIL: {}", e.getMessage());
      if (job != null) {
        duration = job.setEnd();
        job.setStatus(Status.FAILURE);
        objectMapper.writeValue(
            new File(errorDirectory.getAbsolutePath() + "infuser-error-log.json"), job);
      }
      if (sendTextMessages) {
        String msg = String.format("loading *failed* after %s :cow2:", duration);

        try {
          messenger.sendMessage(msg);
        } catch (TextMessengerException ex) {
          logger.warn("Sending message failed: {}", ex.getMessage());
        }
      }
      exitStatus = 1;
    }

    // JobSummary of the import action
    if (job != null && job.getJobId() != null) {
      if (job.getStatus() != Status.NONE && job.getStatus() != Status.IDLE) {
        int todo = (job.getJobsTodo() > 1) ? job.getJobsTodo() - 1 : 0;
        logger.info("Current job has finished. Jobs to do: {}", todo);
        JobSummary summary = new JobSummary(job);
        if (sendTextMessages) {
          try {
            messenger.sendMessage(summary.toString());
          } catch (TextMessengerException e) {
            logger.warn("Failed to send message: {}", e.getMessage());
          }
        }
      }
    }
    System.exit(exitStatus);
  }

  private static String getTimeStamp() {
    LocalDateTime now = LocalDateTime.now();
    return now.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
  }

  private void setClient(RestHighLevelClient client) {
    this.client = client;
  }

  private void run() throws IOException {
    if (jobInProgress()) {
      logger.info("Infuser is already busy. Quiting new job now. Please resume later");
      System.exit(0);
    }
    logger.info("Import process has started");
    logger.info("Configuring the loader");

    try (ESClientManager mgr = ESClientManager.newInstance(config)) {
      setClient(mgr.getClient());
      logger.info("Jobs directory: {}", jobsDirectory.getAbsolutePath());
      logger.info("Update files directory: {}", incomingDirectory.getAbsolutePath());
      logger.info("Archive directory: {}", archiveDirectory.getAbsolutePath());

      job.setBatchSize(batchSize);
      logger.info("Batch size: {} documents", batchSize);
      logger.info("Start loading documents");

      load();
    } catch (IOException e) {
      throw new IOException(e.getMessage());
    }
  }

  private void run(String[] args) throws IOException {
    if (jobInProgress()) {
      logger.info("Infuser is already busy. Quiting new job now.");
      System.exit(1);
    }
    logger.info("Configuring the loader");
    try (ESClientManager mgr = ESClientManager.newInstance(config)) {
      setClient(mgr.getClient());

      if (args != null && args.length > 0 && args[0].equals("--activateIndex")) {

        if (args[1] == null || args[1].trim().length() == 0) {
          throw new MissingFormatArgumentException("Missing index name");
        }

        String index = args[1].trim();
        DocumentType documentType = DocumentType.forIndex(index);
        Alias alias = ESUtil.getAlias(index);
        SourceSystem sourceSystem = ESUtil.getSourceSystem(index);

        if (index.length() == 0 || documentType == null || alias == null || sourceSystem == null) {
          throw new IOException(String.format("Unable to activate index: %s", index));
        }

        if (!ESUtil.indexExists(client, index)) {
          throw new IOException(
              (String.format(
                  "Index \"%s\" does NOT exists. Please provide an exisiting index name", index)));
        }

        logger.info("Activating index: {}", index);
        String backupIndex = getCurrentIndexName(documentType, sourceSystem.toString());
        boolean isBackupAvailable = false;

        if (backupIndex != null) {
          isBackupAvailable = true;
          logger.info("Current index in use is: {}", backupIndex);
        }

        if (ESUtil.addAliasToIndex(client, index, alias)) {
          logger.info("Index {} has been activated", index);
        } else {
          throw new IOException(String.format("Unable to activate index: %s", index));
        }

        if (isBackupAvailable) {
          if (ESUtil.removeAliasesFromIndex(client, backupIndex)) {
            logger.info("Index {} has been de-activated", backupIndex);
          }
        }
      }
    } catch (IOException e) {
      throw new IOException(e.getMessage());
    }
  }

  private void load() throws IOException {
    Map<DocumentType, TreeSet<File>> updateFiles = getUpdateFiles();
    if (updateFiles.isEmpty()) {
      logger.info("No updates to perform");
      job.setStatus(Status.IDLE);
      job.addComment("No update files");
      String duration = job.setEnd();
      job.setDuration(duration);
      if (jobFile != null) {
        saveJob();
      }
      return;
    }
    job.setStatus(Status.INPROGRESS);
    String timestamp = getTimeStamp();
    for (DocumentType documentType : updateFiles.keySet()) {

      // Record what index(es) is in use at the moment
      String currentIndex = getCurrentIndexName(documentType, dataSupplier);
      if (currentIndex != null) {
        job.setBackupIndex(documentType, currentIndex);
        logger.info("Current index \"{}\" set as backup index", currentIndex);
      } else {
        logger.info(
            "No backup index for documents from source: {}, and document type: {}",
            dataSupplier,
            documentType);
      }

      // Create a new index
      // The index name consists of [document type]-[source system]-[timestamp]
      // E.g.: taxon-nsr-20190712114513
      String newIndexName =
          String.format(
              "%s-%s-%s", documentType.name().toLowerCase(), dataSupplier.toLowerCase(), timestamp);
      if (ESUtil.indexExists(client, newIndexName)) {
        throw new IOException(String.format("Index \"%s\" already exists!", newIndexName));
      }
      if (!ESUtil.createIndex(client, newIndexName)) {
        logger.error("Failed to created index {}. Infuser stopped.", newIndexName);
        job.setStatus(Status.FAILURE);
        String duration = job.setEnd();
        job.setDuration(duration);
        saveJob();
        System.exit(1);
      }
      job.setIndexCreated(documentType, newIndexName);

      // Now load all documents for this index from the update files
      for (File file : updateFiles.get(documentType)) {
        logger.info("Processing file: {}", file.getName());
        try {
          importFile(newIndexName, documentType, file);
        } catch (IOException e) {
          logger.warn(
              "File {} was not available for processing! No documents were updated! : {}",
              file.getName(),
              e.getMessage());
        }
      }

      // Check whether loading of all documents of this index was successful?
      int processed = job.getDocumentsProcessed(DocumentType.forIndex(newIndexName));
      int loaded = job.getDocumentsLoaded(DocumentType.forIndex(newIndexName));
      logger.info("{}/{} documents were loaded", loaded, processed);

      if (processed == loaded) {
        // Because there are no errors, the new index can be activated automatically
        // 1. add the alias to the new index ...
        if (ESUtil.addAliasToIndex(client, newIndexName, documentType.getAlias())) {
          job.setIndexCreatedActive(documentType);
          if (job.getStatus() == Status.INPROGRESS) {
            job.setStatus(Status.LOADED);
          }
          ESUtil.updateMetadataIndex(client, documentType);
          logger.info("Index activated: {}", newIndexName);

          // 2. remove the alias from the old index(es)
          if (currentIndex != null) {
            if (ESUtil.removeAliasesFromIndex(client, currentIndex)) {
              job.setBackupIndexNotActive(documentType);
            } else {
              String msg =
                  String.format(
                      "Alias \"%s\" could not be removed from index: %s",
                      documentType.getAlias().getAliasName(), currentIndex);
              logger.warn(msg);
              job.addProcessError(msg);
            }
          }

          // 3. remove all other indexes from this document type and
          //    source system, except(!) the backup index
          Set<String> allIndexes = ESUtil.listIndices(client, DocumentType.forIndex(newIndexName));
          if (cleanUp && !(allIndexes == null || allIndexes.isEmpty())) {
            for (String index : allIndexes) {
              boolean backupIndexPresent = job.getBackupIndex(documentType) != null;
              if (backupIndexPresent
                  && !index.equals(newIndexName)
                  && !job.getBackupIndex(documentType).equals(index)
                  && index.contains(dataSupplier.toLowerCase())) {
                if (ESUtil.deleteIndex(client, index)) {
                  String msg = String.format("Index \"%s\" has been deleted", index);
                  logger.info(msg);
                  job.addComment(msg);
                } else {
                  String msg = String.format("Index \"%s\" could NOT be removed", index);
                  logger.warn(msg);
                  job.addComment(msg);
                  job.addProcessError(msg);
                }
              }
            }
          }
        } else {
          // Unable to add alias to the new index
          job.setStatus(Status.ERRORS);
          job.addProcessError(
              String.format(
                  "Failed to add alias \"%s\" to index \"%s\". Index has NOT been actived!",
                  documentType.getAlias().getAliasName(), newIndexName));
        }
      } else {
        // Not all documents were loaded correctly, so we cannot activate the new index
        job.setStatus(Status.ERRORS);
        String msg = String.format("Index could NOT be activated: %s", newIndexName);
        logger.warn(msg);
        job.addComment(msg);
        job.addProcessError(
            String.format(
                "Not all documents could be loaded. The index was NOT activated: %s",
                newIndexName));
      }
    }

    String duration = job.setEnd();
    job.setDuration(duration);
    saveJob();
  }

  /*
   * Load documents from the import file into the document store
   */
  private void importFile(String index, DocumentType documentType, File f) throws IOException {

    if (f.getName().contains("delete") || f.getName().contains("kill")) {
      logger.error("Deleting documents is not possible in the Colander Light version!!!");
      throw new IOException(
          String.format(
              "File %s contains deletes which is not allowed in this version of the Colander",
              f.getName()));
    } else {
      logger.info("Populating index: {}", index);
      if (index.startsWith("geo")) {
        this.batchSize = 10;
        logger.info("Reset batch size to 10 for indexing geoareas.");
      }

      indexer = new BulkIndexer(client, documentType, index, job);
      String jsonLine;

      try (BufferedReader br = new BufferedReader(new FileReader(f.getAbsolutePath(), StandardCharsets.UTF_8))) {
        while ((jsonLine = br.readLine()) != null) {
          addToUpdatebatch(jsonLine);
        }
      } catch (Exception e) {
        // TODO
        logger.error(e);
      }
      if (!batch.isEmpty()) {
        indexer.index(batch);
        batch.clear();
      }
      ESUtil.flushIndex(client, index);
    }
    archiveFile(f);
  }

  /*
   * ...
   */
  private void addToUpdatebatch(String json) throws IOException {
    batch.add(json);
    if (batch.size() >= batchSize) {
      try {
        indexer.index(batch);
      } catch (ActionRequestValidationException e) {
        logger.error("ActionRequestValidationException?! Size of current batch is: {}", batch.size());
        int n = 0;
        for (String line : batch) {
          int l = 0;
          if (line != null) {
            l = line.length();
          }
          logger.error("{} : {} ({})", n++, line, l);
        }
      } catch (SocketTimeoutException e) {
        logger.warn("SocketTimeoutException but we'll continue ...");
      } finally {
        batch.clear();
      }
    }
  }

  /*
   * Return a structured map containing the files which hold the documents to be updated.
   *
   * @return map
   */
  private Map<DocumentType, TreeSet<File>> getUpdateFiles() {

    List<String> updateFiles;
    jobFile = getJobFile();

    if (jobFile == null) {
      logger.info("No job file was found");
      job.setStatus(Status.IDLE);
      return Collections.emptyMap();
    }
    logger.info("Using job file: {}", jobFile.getName());
    job.setJobFile(jobFile.getName());

    // Retrieve the update files from the job file
    try {
      updateFiles = listUpdateFiles(jobFile);
      job.setUpdateFiles(updateFiles);
      if (dataSupplier == null) {
        throw new RuntimeException("Data Supplier is missing from the job file");
      }
      logger.info("Supplier: {}", dataSupplier);
      job.setDataSupplier(dataSupplier);
    } catch (IOException ex) {
      logger.error("Failure reading the job file {}: {}", jobFile.getName(), ex.getMessage());
      return Collections.emptyMap();
    }
    if (updateFiles.isEmpty()) {
      return Collections.emptyMap();
    }

    // Create a map containing the update files per index (and document type)
    Map<DocumentType, TreeSet<File>> map = new HashMap<>();
    map.put(SPECIMEN, new TreeSet<>());
    map.put(MULTIMEDIAOBJECT, new TreeSet<>());
    map.put(TAXON, new TreeSet<>());
    map.put(GEOAREA, new TreeSet<>());

    for (String fileName : updateFiles) {
      File f =
          new File(config.getIncomingDirectory().getAbsolutePath().concat("/").concat(fileName));
      if (f.getName().contains(DocumentType.SPECIMEN.getAlias().getAliasName())) {
        map.get(SPECIMEN).add(f);
      } else if (f.getName().contains(DocumentType.MULTIMEDIAOBJECT.getAlias().getAliasName())) {
        map.get(MULTIMEDIAOBJECT).add(f);
      } else if (f.getName().contains(DocumentType.TAXON.getAlias().getAliasName())) {
        map.get(TAXON).add(f);
      } else if (f.getName().contains("geo")) { // TODO: this should be generalised!!!
        map.get(GEOAREA).add(f);
      }
    }

    // We're done. Return te map
    logger.info("Found {} specimen files", map.get(SPECIMEN).size());
    logger.info("Found {} multimedia files", map.get(MULTIMEDIAOBJECT).size());
    logger.info("Found {} taxon files", map.get(TAXON).size());
    logger.info("Found {} geoarea files", map.get(GEOAREA).size());

    // Remove indexes which are empty
    Map<DocumentType, TreeSet<File>> resultMap = new HashMap<>();
    for (DocumentType key : map.keySet()) {
      if (!map.get(key).isEmpty()) {
        resultMap.put(key, map.get(key));
      }
    }
    return resultMap;
  }

  /*
   * Private method to archive job and update files
   */
  private void archiveFile(File file) {
    String subFolder = "files";
    if (file.equals(jobFile)) {
      subFolder = "jobs";
    }

    File archiveFolder = new File(archiveDirectory, subFolder);
    if (!archiveFolder.exists()) {
      if (archiveFolder.mkdir()) {
        logger.info("Archive directory created: {}", archiveFolder.getAbsolutePath());
      } else {
        logger.warn("Creation of archive directory failed: {}", archiveFolder.getName());
      }
    }

    try {
      String nameFinishedFile =
          file.getName().replace(".inprogress", ""); // Only relevant for job files
      Files.move(
          file.toPath(), new File(archiveFolder, nameFinishedFile).toPath(), REPLACE_EXISTING);
      logger.info("Archived file to: {}/{}", archiveFolder.getAbsolutePath(), nameFinishedFile);
    } catch (IOException e) {
      logger.error("Failed to archive file: {}. Check the configuration setup!", file.getName());
      logger.error(e.getMessage());
    }
  }

  /*
   * Collect the job files and return the "oldest"
   */
  private File getJobFile() {

    Map<String, File> jobsMap = new HashMap<>();
    File[] jobFiles = listJsonFiles(config.getJobsDirectory());

    // Select the oldest! job file by creating a map with date/time stamp as key
    if (jobFiles.length > 0) {
      for (File file : jobFiles) {
        // Find out what the oldest job file is
        // We're working with that one and leave the rest for later
        // 2019-04-11-123206363
        // String pattern = ".*(\\d{4}[-]\\d{2}[-]\\d{2}[-]\\d{9}).*";
        String pattern = ".*(\\d{4}-\\d{2}-\\d{2}-\\d{9}).*";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(file.getName());

        // If we have more than one job file with exactly the same timestamp
        // we'll keep only one. Since we're only interested in the oldest job
        // file, it doesn't matter if there's more than one. We just need one
        // file that can be considered the oldest.
        if (m.find()) {
          jobsMap.put(m.group(1), file);
        }
      }
      if (jobsMap.size() > 0) {
        job.addJobsTodo(jobsMap.size());
        List<String> keys = new ArrayList<>(jobsMap.size());
        keys.addAll(jobsMap.keySet());
        Collections.sort(keys);

        File jobToDo = jobsMap.get(keys.get(0));
        File inProgress = new File(jobToDo.getAbsolutePath().concat(".inprogress"));
        if (jobToDo.renameTo(inProgress)) {
          logger.info("Job file: {} is now in progress", inProgress.getName());
        } else {
          logger.error(
              "Job file: {} cannot be renamed. Is there another process already busy?",
              jobToDo.getName());
          return null;
        }
        return inProgress;
      }
    }
    return null;
  }

  /*
   * Return the update files from the job file
   *
   * @param File jobfile
   *
   * @return
   *
   * @throws IOException
   */
  private List<String> listUpdateFiles(File f) throws IOException {

    List<String> updateFiles = new ArrayList<>();
    StringBuilder builder;
    try (BufferedReader reader = new BufferedReader(new FileReader(f))) {
      builder = new StringBuilder();
      String currentLine = reader.readLine();

      while (currentLine != null) {
        builder.append(currentLine);
        builder.append("n");
        currentLine = reader.readLine();
      }
    }
    String json = builder.toString();

    JsonNode jsonNode = objectMapper.readTree(json);
    dataSupplier = jsonNode.get("data_supplier").asText();

    // Collect the number of valid documents according to the validator
    JsonNode validator = jsonNode.path("validator");
    if (!validator.path("specimen").path("results").path("valid_json_docs").isMissingNode()) {
      job.setValidSpecimenDocs(
          validator.path("specimen").path("results").path("valid_json_docs").asInt());
    }
    if (!validator.path("taxon").path("results").path("valid_json_docs").isMissingNode()) {
      job.setValidTaxonDocs(
          validator.path("taxon").path("results").path("valid_json_docs").asInt());
    }
    if (!validator.path("multimedia").path("results").path("valid_json_docs").isMissingNode()) {
      job.setValidMultimediaDocs(
          validator.path("multimedia").path("results").path("valid_json_docs").asInt());
    }
    if (!validator.path("geo").path("results").path("valid_json_docs").isMissingNode()) {
      job.setValidGeoDocs(validator.path("geo").path("results").path("valid_json_docs").asInt());
    }

    JsonNode validatedOutput = jsonNode.path("validated_output");
    if (validatedOutput == null || validatedOutput.size() == 0) {
      return Collections.emptyList();
    }
    for (JsonNode output : validatedOutput) {
      String fileName = output.asText().trim();
      // We're only interested in the filename; not the whole path
      int beginIndex = fileName.lastIndexOf("/") + 1;
      fileName = fileName.substring(beginIndex);
      if (fileName.length() > 0) {
        updateFiles.add(fileName);
      }
    }

    // Retrieve the harvest date (export date) from the job file
    JsonNode exportData = jsonNode.path("export_date");
    if (exportData != null) {
      if (exportData.get("taxon") != null && exportData.get("taxon").get("date") != null) {
        job.setHarvestDate(TAXON, exportData.get("taxon").get("date").asText().trim());
        logger.info("taxon harvest date: {}", job.getHarvestDateStr(TAXON));
      }
      if (exportData.get("specimen") != null && exportData.get("specimen").get("date") != null) {
        job.setHarvestDate(SPECIMEN, exportData.get("specimen").get("date").asText().trim());
        logger.info("specimen harvest date: {}", job.getHarvestDateStr(SPECIMEN));
      }
      if (exportData.get("multimedia") != null
          && exportData.get("multimedia").get("date") != null) {
        job.setHarvestDate(
            MULTIMEDIAOBJECT, exportData.get("multimedia").get("date").asText().trim());
        logger.info("multimedia harvest date: {}", job.getHarvestDateStr(MULTIMEDIAOBJECT));
      }
      if (exportData.get("geo") != null && exportData.get("geo").get("date") != null) {
        job.setHarvestDate(GEOAREA, exportData.get("geo").get("date").asText().trim());
        logger.info("geoarea harvest date: {}", job.getHarvestDateStr(GEOAREA));
      }
    }

    // Retrieve the location of the ETL log file
    JsonNode importNotes = jsonNode.path("notes");
    if (importNotes != null) {
      if (importNotes.get("specimen") != null
          && importNotes.get("specimen").get("ETL-logile") != null) {
        job.setEtlLogfiles(SPECIMEN, importNotes.get("specimen").get("ETL-logile").asText());
        logger.info("ETL-import log: {}", job.getEtlLogFile(SPECIMEN));
      }
      if (importNotes.get("taxon") != null && importNotes.get("taxon").get("ETL-logile") != null) {
        job.setEtlLogfiles(TAXON, importNotes.get("taxon").get("ETL-logile").asText());
        logger.info("ETL-import log: {}", job.getEtlLogFile(TAXON));
      }
      if (importNotes.get("multimedia") != null
          && importNotes.get("multimedia").get("ETL-logile") != null) {
        job.setEtlLogfiles(
            MULTIMEDIAOBJECT, importNotes.get("multimedia").get("ETL-logile").asText());
        logger.info("ETL-import log: {}", job.getEtlLogFile(MULTIMEDIAOBJECT));
      }
      if (importNotes.get("geoarea") != null
          && importNotes.get("geoarea").get("ETL-logile") != null) {
        job.setEtlLogfiles(GEOAREA, importNotes.get("geoarea").get("ETL-logile").asText());
        logger.info("ETL-import log: {}", job.getEtlLogFile(GEOAREA));
      }
    }

    return (!updateFiles.isEmpty()) ? updateFiles : Collections.emptyList();
  }

  /*
   * Return a List of files with the extension .json
   *
   * @param directoryName
   *
   */
  private File[] listJsonFiles(File directory) {

    if (!directory.exists()) {
      return new File[0];
    }

    String ext = ".json";
    File[] files = directory.listFiles((f, s) -> s.toLowerCase().endsWith(ext));
    if (files == null) {
      return new File[0];
    }
    Arrays.sort(files);
    return files;
  }

  /**
   * jobInProgress.
   *
   * @return true when a job is in progress
   * @throws IOException when the job directory does not exist
   */
  private boolean jobInProgress() throws IOException {
    File directory = config.getJobsDirectory();
    if (!directory.exists()) {
      throw new IOException(
          String.format("Job directory does not exist: %s", directory.getAbsolutePath()));
    }
    String ext = ".inprogress";
    File[] files = directory.listFiles((f, s) -> s.toLowerCase().endsWith(ext));
    return (files != null && files.length > 0);
  }

  private String getCurrentIndexName(DocumentType documentType, String dataSupplier)
      throws IOException {
    return ESUtil.getActiveIndex(client, documentType, dataSupplier);
  }

  private void saveJob() throws IOException {
    if (!job.getStatus().equals(Status.INPROGRESS)) {
      job.setJobFile(job.getJobFile().replace(".inprogress", ""));
    }
    Map<String, Object> map;
    try {
      map = objectMapper.readValue(jobFile, new TypeReference<Map<String, Object>>() {});
      if (map.get("infuser") != null) {
        map.remove("infuser");
      }
      map.put("infuser", job);
      objectMapper.writeValue(jobFile, map);
      archiveFile(jobFile);
    } catch (IOException e) {
      throw new IOException("Failed to write job file");
    }
  }
}

package nl.naturalis.colander.infuser;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * Simple class for sending messages to an Instant Text Messenger like Slack or Mattermost.
 *
 * Usage:
 *
 * payload={"text": "This is a line of text in a channel.\nAnd this is another line of text."}
 * <p>
 * payload={@code {"text": "{A very important thing has occurred! <https://alert-system.com/alerts/1234|Click here> for details!"} }
 * <p>
 * payload={@code {\"channel\": \"#general\", \"username\": \"webhookbot\", \"text\":
 * \"This is posted to #general and comes from a bot named webhookbot.\", \"icon_emoji\":
 * \":ghost:\"} }
 * <p>
 * Direct Message with "channel": "@username"
 *
 */
@SuppressWarnings("CheckStyle")
public class TextMessenger {

  private final LoaderConfig config;

  public TextMessenger(LoaderConfig config) {
    this.config = config;
  }

  public void sendMessage(String message) throws TextMessengerException {
    String payload = String.format("payload={\"text\": \"*Infuser*: %s\"}", message);
    postToMessenger(payload);
  }

  @SuppressWarnings("unused")
  public void sendDM(String userName, String message) throws TextMessengerException {
    String payload =
        String.format(
            "payload={\"channel\": \"@%s\", \"text\": \"*Infuser*: %s\"}", userName, message);
    postToMessenger(payload);
  }

  private void postToMessenger(String payload) throws TextMessengerException {
    String url = config.getTextMessageHost();
    try {
      StringEntity entity = new StringEntity(payload, ContentType.APPLICATION_FORM_URLENCODED);

      HttpClient httpClient = HttpClientBuilder.create().build();
      HttpPost request = new HttpPost(url);
      request.setEntity(entity);

      HttpResponse response = httpClient.execute(request);
      if (response.getStatusLine().getStatusCode() >= 400) {
        throw new TextMessengerException(
            "Failed to send message: " + response.getStatusLine().getReasonPhrase());
      }
    } catch (IOException | TextMessengerException e) {
      throw new TextMessengerException("Failed to send message", e);
    }
  }
}

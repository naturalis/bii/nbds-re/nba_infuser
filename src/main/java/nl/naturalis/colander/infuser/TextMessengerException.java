package nl.naturalis.colander.infuser;

public class TextMessengerException extends Exception {

  public TextMessengerException(String message, Throwable cause) {
    super(message, cause);
  }

  public TextMessengerException(String message) {
    super(message);
  }

  public TextMessengerException(Throwable cause) {
    super(cause);
  }
}

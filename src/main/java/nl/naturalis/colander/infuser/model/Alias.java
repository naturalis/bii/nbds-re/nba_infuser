package nl.naturalis.colander.infuser.model;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * The alias name mirrors the index name as it is known in the NBA service. So the Specimen
 * document type in the NBA has an index name "specimen".
 *
 * <p>The infuser put each import in a unique index, with a name composed of document type,
 * source system, and a type stamp. E.g. : specimen-xc-20190810040031.  So, for one document
 * type, there can be more indexes. The REST api expects just one index name. This
 * is where aliases come into play. We can assign the same alias name to several indexes,
 * hence use the alias to search all indexes of a specific document type.
 *
 * <p>This enum contains the allowed alias names (index names can be retrieved from the
 * nba.properties file from the NBA.
 *
 * <p>More: https://www.elastic.co/guide/en/elasticsearch/guide/current/index-aliases.html
 */
public enum Alias {
  GEOAREAS ("geoareas"),
  MULTIMEDIA ("multimedia"),
  SPECIMEN ("specimen"),
  TAXON ("taxon");
  
  private final String aliasName;
  
  Alias(String aliasName) {
    this.aliasName = aliasName;
  }
  
  public String getAliasName() {
    return this.aliasName;
  }

  @JsonValue
  public String toString() {
    return aliasName;
  }
  
}

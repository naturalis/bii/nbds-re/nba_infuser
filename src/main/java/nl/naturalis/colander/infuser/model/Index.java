package nl.naturalis.colander.infuser.model;

import com.fasterxml.jackson.annotation.JsonGetter;

/**
 * Class for modeling an index and use in the Job class only.
 */
class Index {

  private final String indexName;
  private boolean active;

  Index(String indexName) {
    this.indexName = indexName;
    this.active = false;
  }

  Index(String indexName, boolean activated) {
    this.indexName = indexName;
    this.active = activated;
  }

  @JsonGetter("indexName")
  String getIndexName() {
    return indexName;
  }

  @JsonGetter("active")
  boolean isActive() {
    return active;
  }

  void setActive(boolean active) {
    this.active = active;
  }
}

package nl.naturalis.colander.infuser.model;

import java.util.Map;
import java.util.Set;

public class IndexMap {
  
  private final Map<String, Map<String, Map<String, Object>>> indices;
  
  public IndexMap(Map<String, Map<String, Map<String, Object>>> map) {
    this.indices = map;
  }
  
  public Set<String> listIndices() {
    return indices.keySet();
  }

  // TODO: maybe a method that lists all aliases in use
}
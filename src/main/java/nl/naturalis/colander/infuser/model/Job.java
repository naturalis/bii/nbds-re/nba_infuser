package nl.naturalis.colander.infuser.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * JobLog for nba loading jobs.
 *
 */
public class Job {

  private static final Logger logger = LogManager.getLogger(Job.class);

  private static final Job instance = new Job();
  private String jobId;
  private String jobFile;
  private List<String> updateFiles;
  private int jobsTodo = 0;
  private final OffsetDateTime begin;
  private OffsetDateTime end;
  private String duration;
  private int batchSize;
  private boolean cleanUp = true;
  private boolean addGeoShape = false;
  private boolean sendTextMessage = false;
  private Status status = Status.NONE;
  private String dataSupplier;
  private Map<DocumentType, String> harvestDates;
  private Map<DocumentType, String> etlLogfiles;
  private Map<DocumentType, Index> indexCreated;
  private Map<DocumentType, Index> indexBackup;
  private int validSpecimenDocs = 0;
  private int validTaxonDocs = 0;
  private int validMultimediaDocs = 0;
  private int validGeoDocs = 0;
  private int specimenDocumentsProcessed = 0;
  private int taxonDocumentsProcessed = 0;
  private int multimediaDocumentsProcessed = 0;
  private int geoareaDocumentsProcessed = 0;
  private int totalDocumentsProcessed = 0;
  private int specimenDocumentsLoaded = 0;
  private int taxonDocumentsLoaded = 0;
  private int multimediaDocumentsLoaded = 0;
  private int geoareaDocumentsLoaded = 0;
  private int totalDocumentsLoaded = 0;
  private int specimenDocumentsDeleted = 0;
  private int taxonDocumentsDeleted = 0;
  private int multimediaDocumentsDeleted = 0;
  private int geoareaDocumentsDeleted = 0;
  private int totalDocumentsDeleted = 0;
  private int errors = 0;
  private final Map<DocumentType, List<LoadError>> loadErrors = new HashMap<>();
  private final Map<DocumentType, List<ParseError>> parseErrors = new HashMap<>();
  private final List<String> processErrors = new ArrayList<>();
  private final List<String> comments = new ArrayList<>();

  private Job() {
    this.begin = OffsetDateTime.now();
  }

  public static Job getInstance() {
    return instance;
  }

  public String getJobId() {
    return jobId;
  }

  public String getJobFile() {
    return jobFile;
  }

  public void setJobFile(String fileName) {
    if (fileName.startsWith("jobs/")) {
      fileName = fileName.substring(5);
    }
    this.jobFile = fileName;
    this.jobId = fileName.substring(0, fileName.lastIndexOf("."));
  }

  public List<String> getUpdateFiles() {
    return updateFiles;
  }

  public void setUpdateFiles(List<String> updateFiles) {
    this.updateFiles = updateFiles;
  }

  public String getComments() {
    return Arrays.toString(comments.toArray());
  }

  public void addComment(String comments) {
    this.comments.add(comments);
  }

  public String getBegin() {
    return begin.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
  }

  @JsonIgnore
  public OffsetDateTime getImportDate() {
    return begin;
  }

  public String getEnd() {
    return end.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
  }

  public String setEnd() {
    this.end = OffsetDateTime.now();
    long duration = Duration.between(begin, end).toMillis();
    long millis = duration % 1000;
    long second = (duration / 1000) % 60;
    long minute = (duration / (1000 * 60)) % 60;
    long hour = (duration / (1000 * 60 * 60)) % 24;
    return String.format("%02d:%02d:%02d.%d", hour, minute, second, millis);
  }

  @JsonGetter
  String getDuration() {
    return duration;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  public int getBatchSize() {
    return batchSize;
  }

  public void setBatchSize(int batchSize) {
    this.batchSize = batchSize;
  }

  @JsonGetter
  public boolean sendTxtMessage() {
    return sendTextMessage;
  }

  public void setSendTextMessage(boolean sendTextMessage) {
    this.sendTextMessage = sendTextMessage;
  }

  @JsonGetter
  public boolean cleanUp() {
    return this.cleanUp;
  }

  public void cleanUp(boolean cleanUp) {
    this.cleanUp = cleanUp;
  }


  @JsonGetter
  public boolean addGeoShape() {
    return this.addGeoShape;
  }

  public void addGeoShape(boolean add) {
    this.addGeoShape = add;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public String getDataSupplier() {
    return dataSupplier;
  }

  public void setDataSupplier(String dataSupplier) {
    this.dataSupplier = dataSupplier;
  }

  public int getJobsTodo() {
    return jobsTodo;
  }

  public void addJobsTodo(int size) {
    jobsTodo = size;
  }

  int getValidDocs(DocumentType documentType) {
    switch (documentType) {
      case SPECIMEN:
        return validSpecimenDocs;
      case TAXON:
        return validTaxonDocs;
      case MULTIMEDIAOBJECT:
        return validMultimediaDocs;
      case GEOAREA:
        return validGeoDocs;
    }
    return 0;
  }

  public void setValidSpecimenDocs(int validSpecimenDocs) {
    this.validSpecimenDocs = validSpecimenDocs;
  }

  public void setValidTaxonDocs(int validTaxonDocs) {
    this.validTaxonDocs = validTaxonDocs;
  }

  public void setValidMultimediaDocs(int validMultimediaDocs) {
    this.validMultimediaDocs = validMultimediaDocs;
  }

  public void setValidGeoDocs(int validGeoDocs) {
    this.validGeoDocs = validGeoDocs;
  }

  public void setIndexCreated(DocumentType documentType, String indexName) {
    if (indexCreated == null) {
      indexCreated = new HashMap<>(4);
    }
    indexCreated.put(documentType, new Index(indexName));
  }

  public void setIndexCreatedActive(DocumentType documentType) {
    if (indexCreated == null || !indexCreated.containsKey(documentType)) {
      throw new RuntimeException(
          "You cannot set an index to active when the index has not been created yet");
    }
    indexCreated.get(documentType).setActive(true);
  }

  Map<DocumentType, Index> getIndexCreated() {
    return this.indexCreated;
  }

  public void setBackupIndex(DocumentType documentType, String indexName) {
    if (indexBackup == null) {
      indexBackup = new HashMap<>(4);
    }
    indexBackup.put(documentType, new Index(indexName, true));
  }

  public void setHarvestDate(DocumentType documentType, String harvestDate) {
    if (harvestDates == null) {
      harvestDates = new HashMap<>(4);
    }
    harvestDates.put(documentType, harvestDate);
  }

  public Map<DocumentType, String> getHarvestDates() {
    return harvestDates;
  }

  public String getHarvestDateStr(DocumentType documentType) {
    if (harvestDates != null && harvestDates.containsKey(documentType)) {
      return harvestDates.get(documentType);
    }
    return null;
  }

  public void setEtlLogfiles(DocumentType documentType, String etlLogfile) {
    if (etlLogfiles == null) {
      etlLogfiles = new HashMap<>(4);
    }
    etlLogfiles.put(documentType, etlLogfile);
  }

  public Map<DocumentType, String> getEtlLogfiles() {
    return etlLogfiles;
  }

  public String getEtlLogFile(DocumentType documentType) {
    if (etlLogfiles != null && etlLogfiles.containsKey(documentType)) {
      return etlLogfiles.get(documentType);
    }
    return null;
  }

  public void setBackupIndexNotActive(DocumentType documentType) {
    if (indexBackup == null || !indexBackup.containsKey(documentType)) {
      throw new RuntimeException(
          "You cannot set a backup index to deactive when the index has not been created yet");
    }
    indexBackup.get(documentType).setActive(false);
  }

  Map<DocumentType, Index> getIndexBackup() {
    return indexBackup;
  }

  public String getBackupIndex(DocumentType documentType) {
    if (indexBackup == null) return null;
    if (indexBackup.containsKey(documentType)) {
      return indexBackup.get(documentType).getIndexName();
    }

    return null;
  }

  public void addDocumentsProcessed(DocumentType documentType, int processed) {
    totalDocumentsProcessed += processed;
    if (documentType.isSpecimen()) {
      specimenDocumentsProcessed += processed;
    } else if (documentType.isTaxon()) {
      taxonDocumentsProcessed += processed;
    } else if (documentType.isMultiMediaObject()) {
      multimediaDocumentsProcessed += processed;
    } else if (documentType.isGeoArea()) {
      geoareaDocumentsProcessed += processed;
    } else {
      logger.error(
          "Unknown index \"{}\". Number of unspecified processed documents: {}",
          documentType,
          processed);
    }
  }

  public int getDocumentsProcessed(DocumentType documentType) {
    if (documentType.isSpecimen()) {
      return specimenDocumentsProcessed;
    } else if (documentType.isTaxon()) {
      return taxonDocumentsProcessed;
    } else if (documentType.isMultiMediaObject()) {
      return multimediaDocumentsProcessed;
    } else if (documentType.isGeoArea()) {
      return geoareaDocumentsProcessed;
    }
    return 0;
  }

  public int getTotalDocumentsProcessed() {
    return totalDocumentsProcessed;
  }

  public void addDocumentsLoaded(DocumentType documentType, int loaded) {
    if (documentType.isSpecimen()) {
      specimenDocumentsLoaded += loaded;
    } else if (documentType.isTaxon()) {
      taxonDocumentsLoaded += loaded;
    } else if (documentType.isMultiMediaObject()) {
      multimediaDocumentsLoaded += loaded;
    } else if (documentType.isGeoArea()) {
      geoareaDocumentsLoaded += loaded;
    } else {
      logger.error(
          "Unknown index \"{}\". Number of unspecified updated documents: {}",
          documentType,
          loaded);
    }
  }

  public int getTotalDocumentsLoaded() {
    return specimenDocumentsLoaded
        + taxonDocumentsLoaded
        + multimediaDocumentsLoaded
        + geoareaDocumentsLoaded;
  }

  public int getDocumentsLoaded(DocumentType documentType) {
    if (documentType.isSpecimen()) {
      return specimenDocumentsLoaded;
    } else if (documentType.isTaxon()) {
      return taxonDocumentsLoaded;
    } else if (documentType.isMultiMediaObject()) {
      return multimediaDocumentsLoaded;
    } else if (documentType.isGeoArea()) {
      return geoareaDocumentsLoaded;
    }
    return 0;
  }

  public void addDocumentsDeleted(DocumentType documentType, int deleted) {
    totalDocumentsDeleted += deleted;
    if (documentType.isSpecimen()) {
      specimenDocumentsDeleted += deleted;
    } else if (documentType.isTaxon()) {
      taxonDocumentsDeleted += deleted;
    } else if (documentType.isMultiMediaObject()) {
      multimediaDocumentsDeleted += deleted;
    } else if (documentType.isGeoArea()) {
      geoareaDocumentsDeleted += deleted;
    } else {
      logger.error(
          "Unknown index \"{}\". Number of unspecified deleted documents: {}",
          documentType,
          deleted);
    }
  }

  int getDocumentsDeleted(DocumentType documentType) {
    if (documentType.isSpecimen()) {
      return specimenDocumentsDeleted;
    } else if (documentType.isTaxon()) {
      return taxonDocumentsDeleted;
    } else if (documentType.isMultiMediaObject()) {
      return multimediaDocumentsDeleted;
    } else if (documentType.isGeoArea()) {
      return geoareaDocumentsDeleted;
    }
    return 0;
  }

  public void addParseError(DocumentType documentType, ParseError error) {
    if (error == null) {
      return;
    }
    if (parseErrors.get(documentType) == null) {
      parseErrors.put(documentType, new ArrayList<>());
    }
    parseErrors.get(documentType).add(error);
  }

  public List<ParseError> getParseErrors(DocumentType documentType) {
    return parseErrors.get(documentType);
  }

  public Map<DocumentType, List<ParseError>> getParseErrors() {
    return this.parseErrors;
  }

  public void addLoadError(DocumentType documentType, LoadError loadError) {
    if (loadError == null) {
      return;
    }
    if (loadErrors.get(documentType) == null) {
      loadErrors.put(documentType, new ArrayList<>());
    }
    loadErrors.get(documentType).add(loadError);
  }

  public void addLoadErrors(DocumentType documentType, List<LoadError> loadErrors) {
    if (loadErrors == null) {
      return;
    }
    if (this.loadErrors.get(documentType) == null) {
      this.loadErrors.put(documentType, new ArrayList<>());
    }
    this.loadErrors.get(documentType).addAll(loadErrors);
  }

  public List<LoadError> getLoadErrors(DocumentType documentType) {
    return loadErrors.get(documentType);
  }

  //    public Map<DocumentType, List<LoadError>> getLoadErrors() {
  //        return this.loadErrors;
  //    }

  public String getLoadErrors() {
    StringBuilder str = new StringBuilder();
    for (DocumentType dt : loadErrors.keySet()) {
      String errors = Arrays.toString(loadErrors.get(dt).toArray());
      str.append(dt.toString()).append(":").append(errors);
    }
    return str.toString();
  }

  public void addProcessError(String errorMessage) {
    processErrors.add(errorMessage);
  }

  List<String> getProcessErrors() {
    return this.processErrors;
  }

  public void addToTotalErrors(int n) {
    this.errors += n;
  }

  public int getTotalLoadErrors() {
    return this.errors;
  }

  int getTotalNumberErrors(DocumentType documentType) {

    int totalParseErrors = 0;
    if (parseErrors != null) {
      totalParseErrors +=
          (parseErrors.get(documentType) == null) ? 0 : (parseErrors.get(documentType).size());
    }

    int totalLoadErrors = 0;
    if (loadErrors != null) {
      totalLoadErrors +=
          (loadErrors.get(documentType) == null) ? 0 : (loadErrors.get(documentType).size());
    }
    return totalParseErrors + totalLoadErrors;
  }
}

package nl.naturalis.colander.infuser.model;

import com.fasterxml.jackson.annotation.JsonValue;
import java.util.Objects;

/**
 * A LoadError is an error that can occur when documents are pushed to Elasticsearch.
 *
 */
public class LoadError {

  private final String id;
  private final String type;
  private final String reason;

  public LoadError(String id, String type, String reason) {
    this.id = Objects.requireNonNull(id, "id cannot be left null");
    this.type = type;
    this.reason = reason;
  }

  public String getId() {
    return id;
  }

  public String getType() {
    return type;
  }

  public String getReason() {
    return reason;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.format(
        "Error loading document with id: %s - error: %s, reason: %s", id, type, reason);
  }
}

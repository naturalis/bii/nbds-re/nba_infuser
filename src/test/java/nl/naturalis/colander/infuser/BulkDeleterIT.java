package nl.naturalis.colander.infuser;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.elasticsearch.client.RestHighLevelClient;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Integration Test for the BulkDeleter
 */
public class BulkDeleterIT {

  private static final Logger logger = LogManager.getLogger(BulkDeleterIT.class);
  
  // TODO: for these tests to work, (embedded) Elasticsearch needs to be implemented first
  
  @BeforeClass
  public static void before() throws Exception
  {
    //
  }
  
  @Ignore
  @Test
  public void testCreateIndex() throws IOException {
    LoaderConfig config = LoaderConfig.getInstance();
    ESClientManager mgr = ESClientManager.newInstance(config);
    RestHighLevelClient client = mgr.getClient();
    BulkDeleter bd = new BulkDeleter();
    // ...
  }
  
  
}

package nl.naturalis.colander.infuser;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import nl.naturalis.colander.infuser.model.DocumentType;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class DocumentTypeTest {

  @BeforeClass
  public static void before()
  {}

  @AfterClass
  public static void after()
  {}

  @Test
  public void testIsDocumentType() {

    DocumentType geoAreaType = DocumentType.GEOAREA;
    assertTrue(geoAreaType.isGeoArea());
    assertFalse(geoAreaType.isTaxon());

    DocumentType multimediaType = DocumentType.MULTIMEDIAOBJECT;
    assertTrue(multimediaType.isMultiMediaObject());

    DocumentType specimenType = DocumentType.SPECIMEN;
    assertTrue(specimenType.isSpecimen());
    assertFalse(specimenType.isMultiMediaObject());

    DocumentType taxonType = DocumentType.TAXON;
    assertTrue(taxonType.isTaxon());
  }

}
